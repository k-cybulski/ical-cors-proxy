#!/usr/bin/env python3

import requests
from flask import Flask, request
from flask_cors import CORS
# strong assumption that ics version is 0.7, since this is an internal api that
# was changed later
from ics.grammar.parse import calendar_string_to_containers, ParseError
from tatsu.exceptions import FailedParse

app = Flask(__name__)
CORS(app)

@app.route('/', methods=['GET'])
def proxy():
    path = request.full_path[2:]
    try:
        response = requests.get(path).content.decode('utf-8')
    except:
        return ("Failed to connect", 500)
    try:
        calendar_string_to_containers(response)
        return (response, 200)
    except (ParseError, FailedParse):
        return ("Failed parsing ICS calendar :(", 403)
    except:
        return ("An error has occured somewhere", 500)

if __name__ == '__main__':
    app.run(debug=True)
