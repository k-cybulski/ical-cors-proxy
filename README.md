# ICAL CORS proxy
This tiny web app works as a proxy which allows ics requests to pass through to
ignore CORS limitations.

When you execute
```
gunicorn app:app
```
you will have it running locally.

To then request a calendar via proxy, write the following address:
```
<proxy server name>/?<url of the calendar>
```

## Caveats
Due to parsing calendars to ensure they're ical before sending them over, this proxy
is quite slow.
